import axios from 'axios'
import { apiUrl } from '../config.js'

export class ProductsClient {
  constructor (token) {
    this.client = axios.create({
      baseURL: apiUrl + '/v1/'
    })
    this.client.interceptors.request.use(function (config) {
      config.headers.Authorization = `Bearer ${token}`
      return config
    })
  }

  async listGroups () {
    return this.client.get('/groups/')
  }

  async createGroup (name) {
    return this.client.post('/groups/', { name: name })
  }

  async deleteGroup (group) {
    return this.client.delete(`/groups/${group.id}`, {})
  }

  async addProduct (group, url) {
    return this.client.post(`/groups/${group.id}/products/`, { url: url })
  }

  async removeProduct (group, product) {
    return this.client.delete(`/groups/${group.id}/products/${product.id}`, {})
  }

  async getHistory (group, product) {
    return this.client.get(`/groups/${group.id}/products/${product.id}/history/`)
  }
}
