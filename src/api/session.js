import { googleClientId } from '../config.js'

export function getGoogleUser (auth2) {
  const currentUser = auth2.currentUser.get()
  if (currentUser.isSignedIn()) {
    return currentUser
  }
  return null
}

export async function loginGoogle (auth2) {
  return auth2.signIn()
}

export function loadGapi (callback) {
  window.gapi.load('auth2', () => {
    if (!window.gapi.auth2.getAuthInstance()) {
      window.gapi.auth2.init({ client_id: googleClientId }).then(callback)
    }
    callback(window.gapi.auth2.getAuthInstance())
  })
}
