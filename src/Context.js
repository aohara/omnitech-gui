import React, { Component } from 'react'

import FuzzySearch from 'fuzzy-search'
import { ProductsClient } from './api/productsClient.js'

export const Context = React.createContext()

export class Provider extends Component {
  constructor (props) {
    super(props)

    this.state = {
      user: null,
      groups: [],
      visibleGroups: [],
      product: null,
      isLoading: false
    }
  }

  async sync () {
    this.setState({ isLoading: true })

    const client = await this.getClient()
    const response = await client.listGroups()
    const groups = response.data

    this.setState({ groups, isLoading: false, visibleGroups: groups })
  }

  async createGroup (name) {
    const client = await this.getClient()
    const response = await client.createGroup(name)

    const newGroup = response.data
    this.setState(oldState => {
      const groups = oldState.groups.concat(newGroup)
      const visibleGroups = oldState.visibleGroups.concat(newGroup)
      return { groups, visibleGroups }
    })
    return newGroup
  }

  async deleteGroup (group) {
    const client = await this.getClient()
    await client.deleteGroup(group)
    this.setState((oldState) => {
      const groups = oldState.groups
        .filter(g => g.id !== group.id)

      return { groups, visibleGroups: groups, selectedGroup: null }
    })
  }

  async addProduct (group, url) {
    const client = await this.getClient()
    const response = await client.addProduct(group, url)
    const updatedGroup = response.data

    this.setState((oldState) => {
      const groups = oldState.groups
        .map(g => g.id === updatedGroup.id ? updatedGroup : g)
      return { groups, visibleGroups: groups, selectGroup: updatedGroup }
    })
    this.search('')
  }

  async removeProduct (group, product) {
    const client = await this.getClient()
    const response = await client.removeProduct(group, product)
    const updatedGroup = response.data

    this.setState((oldState) => {
      const groups = oldState.groups
        .map(g => g.id === updatedGroup.id ? updatedGroup : g)
      return { groups, visibleGroups: groups, selectedGroup: updatedGroup }
    })
  }

  search (term) {
    this.setState((oldState) => {
      const searcher = new FuzzySearch(oldState.groups, ['name'])
      const result = searcher.search(term)

      return { visibleGroups: result, selectedGroup: null }
    })
  }

  setUser (user) {
    if (user) {
      this.setState({ user })
      this.sync()
    } else {
      this.setState({ user: null, client: null, groups: [], visibleGroups: [], isLoading: false })
    }
  }

  async getClient () {
    const { user } = this.state

    const expiresAt = new Date(user.getAuthResponse().expires_at)
    if (!this.client || expiresAt < new Date()) {
      const authResponse = await user.reloadAuthResponse()
      this.client = new ProductsClient(authResponse.id_token)
    }

    return this.client
  }

  async getHistory (group, product) {
    const client = await this.getClient()
    const response = await client.getHistory(group, product)
    return response.data
  }

  selectGroup (selectedGroup) {
    this.setState({ selectedGroup })
  }

  render () {
    const { children } = this.props

    return (
      <Context.Provider
        value={{
          user: this.state.user,
          loggedIn: this.state.user !== null,
          groups: this.state.visibleGroups,
          isLoading: this.state.isLoading,
          createGroup: this.createGroup.bind(this),
          deleteGroup: this.deleteGroup.bind(this),
          addProduct: this.addProduct.bind(this),
          removeProduct: this.removeProduct.bind(this),
          search: this.search.bind(this),
          setUser: this.setUser.bind(this),
          sync: this.sync.bind(this),
          getHistory: this.getHistory.bind(this),
          selectGroup: this.selectGroup.bind(this),
          selectedGroup: this.state.selectedGroup
        }}
      >
        {children}
      </Context.Provider>
    )
  }
}
