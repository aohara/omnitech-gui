export const product1 = {
  id: 'store_ca-p1',
  name: 'Product One',
  url: 'fake://store.ca/p1',
  price: '13.37',
  store: 'store_ca'
}

export const group1 = {
  id: '1',
  name: 'Group One',
  products: [ ]
}
