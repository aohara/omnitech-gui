import React, { Component } from 'react'

import CircularProgress from '@material-ui/core/CircularProgress'

import Header from './Header/Header.js'
import GroupList from './Group/GroupList.js'
import GroupDetails from './Group/GroupDetails.js'
import { Provider, Context } from './Context.js'

class Main extends Component {
  render () {
    const { selectedGroup, groups, loggedIn, isLoading } = this.context

    return <div>
      <Header />
      { loggedIn
        ? isLoading
          ? <CircularProgress size={300} />
          : selectedGroup
            ? <GroupDetails group={selectedGroup} />
            : <GroupList groups={groups} />
        : null
      }
    </div>
  }
}
Main.contextType = Context

export default class App extends Component {
  render () {
    return <Provider>
      <Main />
    </Provider>
  }
}
