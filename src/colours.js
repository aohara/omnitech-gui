import purple from '@material-ui/core/colors/purple'
import red from '@material-ui/core/colors/red'
import indigo from '@material-ui/core/colors/indigo'
import blue from '@material-ui/core/colors/blue'
import cyan from '@material-ui/core/colors/cyan'
import green from '@material-ui/core/colors/green'
import orange from '@material-ui/core/colors/orange'
import brown from '@material-ui/core/colors/brown'
import blueGrey from '@material-ui/core/colors/blueGrey'

export const colours = [ purple[500], green[500], red[500], brown[500], cyan[500], indigo[500], orange[500], blueGrey[500], blue[500] ]
