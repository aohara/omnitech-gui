import React, { Component } from 'react'

import IconButton from '@material-ui/core/IconButton'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import DeleteIcon from '@material-ui/icons/Delete'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'

import { Context } from '../Context.js'
import ProductsList from '../Product/ProductsList.js'
import GroupHistory from './GroupHistory.js'

const styles = theme => ({
  graph: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block'
    }
  }
})

class GroupDetails extends Component {
  back () {
    this.context.selectGroup(null)
  }

  delete () {
    const { group } = this.props
    this.context.deleteGroup(group)
  }

  render () {
    const { group, classes } = this.props
    const featured = group.products.sort((a, b) => a.price - b.price)[0]

    return <div>
      <AppBar position='static'>
        <Toolbar>
          <IconButton color='inherit' onClick={this.back.bind(this)}>
            <ArrowBackIcon />
          </IconButton>

          <Typography variant='title' color='inherit'>
            {group.name}
          </Typography>

          <IconButton color='inherit' onClick={this.delete.bind(this)}>
            <DeleteIcon />
          </IconButton>
        </Toolbar>
      </AppBar>

      { featured ? <img src={featured.image_url} style={{ maxWidth: 400, maxHeight: 400 }} alt={group.name} /> : null }

      <div className={classes.graph} >
        <GroupHistory group={group} />
      </div>

      <ProductsList group={group} />
    </div>
  }
}
GroupDetails.contextType = Context
export default withStyles(styles)(GroupDetails)
