import React, { Component } from 'react'

import Grid from '@material-ui/core/Grid'
import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core/styles'

import CreateGroupModal from './CreateGroupModal'
import Group from './Group.js'

const styles = theme => ({
  createGroupButton: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block'
    }
  },
  createGroupFab: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed'
  }
})

class GroupList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      createModalOpen: false
    }
  }

  handleOpenAddProductModal () {
    this.setState({ createModalOpen: true })
  }

  handleCloseAddProductModal () {
    this.setState({ createModalOpen: false })
  }

  render () {
    const { groups, classes } = this.props
    const { createModalOpen } = this.state

    return (
      <div>
        <Grid
          container
          alignItems='center'
          justify='center'
        >
          <Grid item>
            <List>
              <ListItem key='default' className={classes.createGroupButton}>
                <Button
                  variant='contained'
                  color='primary'
                  onClick={this.handleOpenAddProductModal.bind(this)}
                >
                  <AddIcon />
                  Add Product
                </Button>
              </ListItem>

              {groups.map((group) => {
                return <ListItem key={group.id}>
                  <Group
                    key={group.id}
                    group={group}
                  />
                </ListItem>
              })}
            </List>
          </Grid>
        </Grid>

        <Fab color='primary' onClick={this.handleOpenAddProductModal.bind(this)} className={classes.createGroupFab}>
          <AddIcon />
        </Fab>

        <CreateGroupModal
          modalOpen={createModalOpen}
          handleCloseModal={this.handleCloseAddProductModal.bind(this)}
        />
      </div>
    )
  }
}
export default withStyles(styles)(GroupList)
