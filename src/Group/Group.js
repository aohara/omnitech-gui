import React, { Component } from 'react'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Grid from '@material-ui/core/Grid'
import DeleteIcon from '@material-ui/icons/Delete'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import CardActionArea from '@material-ui/core/CardActionArea'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

import { Context } from '../Context.js'

export default class Group extends Component {
  constructor (props) {
    super(props)
    this.state = {
      anchorEl: null
    }
  }

  handleMenuClick (event) {
    this.setState({ anchorEl: event.currentTarget })
  }

  handleClose () {
    this.setState({ anchorEl: null })
  }

  delete () {
    const { group } = this.props
    const { context } = this
    context.deleteGroup(group)
  }

  select () {
    const { group } = this.props
    this.context.selectGroup(group)
  }

  render () {
    const { group } = this.props
    const { anchorEl } = this.state

    const featured = group.products.sort((a, b) => a.price - b.price)[0]

    return <div>
      <Card id={`group_${group.id}`}>
        <CardActionArea onClick={this.select.bind(this)}>
          <Grid container direction={'row'} spacing={0}>
            <Grid item xs>
              { featured ? <img src={featured.image_url} style={{ maxWidth: 200, maxHeight: 200 }} alt={group.name} /> : null }
            </Grid>
            <Grid item xs>
              <CardContent>
                <Typography component='h5' variant='h5'>
                  {group.name}
                </Typography>
                { featured
                  ? <div>
                    <Typography>${featured.price}</Typography>
                    <Button variant='contained' target='_blank' href={featured.url} onClick={event => event.stopPropagation()}>
                      {featured.store}
                    </Button>
                  </div>
                  : null
                }
              </CardContent>
            </Grid>
          </Grid>
        </CardActionArea>
      </Card>

      <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={this.handleClose.bind(this)}
      >
        <MenuItem onClick={this.delete.bind(this)}>
          <ListItemIcon>
            <DeleteIcon />
          </ListItemIcon>
          <ListItemText primary='Delete' />
        </MenuItem>
      </Menu>
    </div>
  }
}
Group.contextType = Context
