import React, { Component } from 'react'

import CircularProgress from '@material-ui/core/CircularProgress'

import { LineChart, Line, XAxis, YAxis, Tooltip, CartesianGrid } from 'recharts'
import moment from 'moment'

import { Context } from '../Context.js'
import { colours } from '../colours.js'

export default class GroupHistory extends Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: true,
      history: []
    }
  }

  async componentDidMount () {
    const { group } = this.props
    const { getHistory } = this.context

    for (const i in group.products) {
      const product = group.products[i]
      const history = await getHistory(group, product)

      const convertedHistory = history.map(h => {
        const h2 = { time: new Date(h.timestamp).valueOf() }
        h2[product.id] = h.price
        return h2
      })
      this.setState(prevState => {
        const newHistory = prevState.history
          .concat(convertedHistory)
          .sort((a, b) => a.timestamp - b.timestamp)
        return { history: newHistory }
      })
    }

    this.setState({ loading: false })
  }

  RenderHistory () {
    const { group } = this.props
    const { history } = this.state

    return <LineChart width={800} height={400} data={history} >
      <CartesianGrid vertical={false} />
      <XAxis
        domain={['auto', 'auto']}
        dataKey='time'
        tickFormatter={timeStr => moment(timeStr).format('MMM Do')}
        type='number'
      />
      <YAxis domain={['auto', 'auto']} />
      <Tooltip />
      {group.products.map((product, i) => {
        const colour = colours[i]
        return <Line key={product.id} dataKey={product.id} stroke={colour} isAnimationActive={false} />
      })}
    </LineChart>
  }

  render () {
    const { loading } = this.state

    return <div>
      { loading
        ? <CircularProgress size={300} />
        : this.RenderHistory()
      }
    </div>
  }
}

GroupHistory.contextType = Context
