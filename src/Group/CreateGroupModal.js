import React, { Component } from 'react'

import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Button from '@material-ui/core/Button'
import { Context } from '../Context.js'

export default class CreateGroupModal extends Component {
  constructor (props) {
    super(props)
    this.state = {
      name: '',
      url: ''
    }
  }

  async handleSubmit () {
    const { name, url } = this.state
    const { handleCloseModal } = this.props
    const { createGroup, addProduct } = this.context

    if (name.length === 0) {
      return
    }

    handleCloseModal()

    const product = await createGroup(name)
    if (url.length !== 0) {
      addProduct(product, url)
    }
  }

  onEntering () {
    this.setState({ name: '', url: '' })
  }

  render () {
    const { modalOpen, handleCloseModal } = this.props
    const { name, url } = this.state

    return (
      <Dialog
        open={modalOpen}
        onClose={handleCloseModal}
        onEntering={this.onEntering.bind(this)}
      >
        <DialogTitle>Start Tracking a Product</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Start tracking a new product
          </DialogContentText>
          <TextField
            autoFocus
            margin='dense'
            id='name'
            label='Name'
            fullWidth
            required
            onChange={event => this.setState({ name: event.target.value })}
            value={name}
          />
          <TextField
            margin='dense'
            id='url'
            label='url'
            fullWidth
            type='url'
            onChange={event => this.setState({ url: event.target.value })}
            value={url}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseModal} color='secondary'>
            Cancel
          </Button>
          <Button onClick={this.handleSubmit.bind(this)} color='primary'>
            Create
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}
CreateGroupModal.contextType = Context
