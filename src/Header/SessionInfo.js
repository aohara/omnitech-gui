import React, { Component } from 'react'

import Typography from '@material-ui/core/Typography'
import Avatar from '@material-ui/core/Avatar'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'

export default class SessionInfo extends Component {
  constructor (props) {
    super(props)

    this.state = {
      anchorEl: null
    }
  }

  handleOpen (event) {
    this.setState({ anchorEl: event.currentTarget })
  }

  handleClose () {
    this.setState({ anchorEl: null })
  }

  handleLogout () {
    this.handleClose()
    this.props.handleLogout()
  }

  render () {
    const { anchorEl } = this.state
    const open = Boolean(anchorEl)
    const { user } = this.props

    const userProfile = user.getBasicProfile()

    return <span>
      <Typography variant='h6' color='inherit' style={{ display: 'inline-flex', paddingRight: '5px' }}>
        <Avatar src={userProfile.getImageUrl()} onClick={this.handleOpen.bind(this)} />
      </Typography>

      <Menu
        id='menu-appbar'
        anchorEl={anchorEl}
        open={open}
        onClose={(this.handleClose.bind(this))}
      >
        <MenuItem>
          <Avatar src={userProfile.getImageUrl()} />
          {userProfile.getName()}
        </MenuItem>
        <MenuItem onClick={this.handleLogout.bind(this)}>Logout</MenuItem>
      </Menu>
    </span>
  }
}
