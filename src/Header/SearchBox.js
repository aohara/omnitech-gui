import React, { Component } from 'react'

import SearchIcon from '@material-ui/icons/Search'
import InputBase from '@material-ui/core/InputBase'
import InputAdornment from '@material-ui/core/InputAdornment'
import IconButton from '@material-ui/core/IconButton'
import ClearIcon from '@material-ui/icons/Clear'

import { Context } from '../Context.js'

export default class SearchBox extends Component {
  constructor (props) {
    super(props)

    this.state = { term: '' }
  }

  handleSearchUpdate (event) {
    const term = event.target.value
    this.setState({ term })

    this.context.search(term)
  }

  shouldComponentUpdate (nextProps, nextState) {
    return nextState.term !== this.state.term
  }

  handleSearchClear () {
    this.setState({ term: '' })
    this.context.search('')
  }

  render () {
    const { term } = this.state

    return <div>
      <InputBase
        style={{ backgroundColor: 'white' }}
        placeholder='Search…'
        onChange={this.handleSearchUpdate.bind(this)}
        value={term}
        endAdornment={
          <InputAdornment position='end'>
            <IconButton onClick={this.handleSearchClear.bind(this)}>
              { term.length === 0 ? <SearchIcon /> : <ClearIcon /> }
            </IconButton>
          </InputAdornment>
        }
      />
    </div>
  }
}
SearchBox.contextType = Context
