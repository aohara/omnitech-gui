import React, { Component } from 'react'
import classNames from 'classnames'

import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'

import SearchBox from './SearchBox.js'
import Session from './Session.js'
import { Context } from '../Context.js'
import HeaderActions from './HeaderActions.js'

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block'
    }
  },
  headerElement: {
    margin: theme.spacing.unit
  }
})

class Header extends Component {
  render () {
    const { classes } = this.props
    const { loggedIn } = this.context

    return <div className={classes.root}>
      <AppBar position='static'>
        <Toolbar>
          <Typography className={classes.title} variant='title' color='inherit'>
            Omnitech
          </Typography>

          <div className={classNames(classes.grow, classes.headerElement)}>
            {loggedIn ? <SearchBox /> : null }
          </div>

          <HeaderActions className={classes.grow} />
          <Session />
        </Toolbar>
      </AppBar>
    </div>
  }
}
Header.contextType = Context

export default withStyles(styles)(Header)
