import React, { Component } from 'react'

import IconButton from '@material-ui/core/IconButton'
import RefreshIcon from '@material-ui/icons/Refresh'

import { Context } from '../Context.js'

export default class HeaderActions extends Component {
  render () {
    const { sync } = this.context

    return <div>
      <IconButton onClick={sync} color='inherit'>
        <RefreshIcon />
      </IconButton>
    </div>
  }
}
HeaderActions.contextType = Context
