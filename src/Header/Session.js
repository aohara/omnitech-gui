import React, { Component } from 'react'

import Button from '@material-ui/core/Button'

import SessionInfo from './SessionInfo.js'
import { Context } from '../Context.js'
import { loadGapi, loginGoogle, getGoogleUser } from '../api/session.js'

export default class Session extends Component {
  componentDidMount () {
    loadGapi(async (auth2) => {
      const user = await getGoogleUser(auth2)
      if (user) {
        this.loginAs(user)
      }
    })
  }

  startLogin () {
    loadGapi(async (auth2) => {
      const user = await loginGoogle(auth2)
      if (user) {
        this.loginAs(user)
      }
    })
  }

  handleLogout () {
    loadGapi(async (auth2) => {
      auth2.signOut()
      this.context.setUser(null)
    })
  }

  loginAs (user) {
    const { setUser } = this.context
    setUser(user)
  }

  render () {
    const { loggedIn, user } = this.context

    if (loggedIn) {
      return <SessionInfo user={user} handleLogout={this.handleLogout.bind(this)} />
    }
    return <Button
      id='loginButton'
      variant='contained'
      color='primary'
      onClick={this.startLogin.bind(this)}
    >
      Log In
    </Button>
  }
}
Session.contextType = Context
