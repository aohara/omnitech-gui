import React, { Component } from 'react'

import ListItemText from '@material-ui/core/ListItemText'
import ListItem from '@material-ui/core/ListItem'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import CircularProgress from '@material-ui/core/CircularProgress'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'

import { Context } from '../Context.js'

export default class Product extends Component {
  delete () {
    const { group, product } = this.props
    this.context.removeProduct(group, product)
  }

  render () {
    const { product } = this.props

    return <ListItem button target='_blank' href={product.url}>
      { product.fetching ? <CircularProgress /> : <span /> }
      <ListItemText
        data-testid='product-data'
        primary={<span>{product.store} - {product.name}</span>}
        secondary={<span>${product.price}</span>}
      />
      <ListItemSecondaryAction>
        <IconButton onClick={this.delete.bind(this)}>
          <DeleteIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  }
}
Product.contextType = Context
