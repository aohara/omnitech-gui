import React, { Component } from 'react'

import Product from './Product.js'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Button from '@material-ui/core/Button'

import AddProductModal from '../Product/AddProductModal'

export default class ProductsList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      modalOpen: false
    }
  }

  openModal () {
    this.setState({ modalOpen: true })
  }

  closeModal () {
    this.setState({ modalOpen: false })
  }

  render () {
    const { group } = this.props
    const { modalOpen } = this.state

    return <div>
      <List>
        {group.products.map(product => {
          return <Product key={product.id} group={group} product={product} />
        })}

        <ListItem>
          <Button color='primary' onClick={this.openModal.bind(this)}>
            Add Store
          </Button>
        </ListItem>
      </List>

      <AddProductModal
        group={group}
        modalOpen={modalOpen}
        handleClose={this.closeModal.bind(this)}
      />
    </div>
  }
}
