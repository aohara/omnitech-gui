import React, { Component } from 'react'

import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Button from '@material-ui/core/Button'
import { Context } from '../Context.js'

export default class AddProductModal extends Component {
  constructor (props) {
    super(props)
    this.state = { url: '' }
  }

  handleSubmit () {
    const { url } = this.state
    const { group, handleClose } = this.props

    if (url.length > 0) {
      handleClose()
      this.context.addProduct(group, url)
    }
  }

  handleClose () {
    this.setState({ url: '' })
    this.props.handleClose()
  }

  render () {
    const { modalOpen } = this.props
    const { url } = this.state
    const handleClose = this.handleClose.bind(this)

    return (
      <Dialog
        open={modalOpen}
        onClose={handleClose}
      >
        <DialogTitle>Add Store</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin='dense'
            id='url'
            label='url'
            fullWidth
            required
            type='url'
            onChange={event => { this.setState({ url: event.target.value }) }}
            value={url}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color='secondary'>
            Cancel
          </Button>
          <Button onClick={this.handleSubmit.bind(this)} color='primary'>
            Watch
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}
AddProductModal.contextType = Context
