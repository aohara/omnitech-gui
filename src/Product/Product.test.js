/* global expect, test, afterEach */

import React from 'react'
import { render, cleanup } from 'react-testing-library'
import 'jest-dom/extend-expect'

import Product from './Product.js'
import { group1, product1 } from '../__fixtures__/fixtures.js'

afterEach(cleanup)

test('Display a Product', async () => {
  group1.products = [ product1 ]
  const { getByTestId } = render(
    <Product group={group1} product={product1} />
  )

  expect(getByTestId('product-data')).toHaveTextContent('store_ca - Product One$13.37')
})
